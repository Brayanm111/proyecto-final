<?php
include "config.php";
include "utils.php";


$dbConn =  connect($db);

/*
  listar todos los posts o solo uno
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['ID_Cliente']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM usuario where ID_Cliente=:ID_Cliente");
      $sql->bindValue(':ID_Cliente', $_GET['ID_Cliente']);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM usuario");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $sql = "INSERT INTO usuario
          (ID_Usuario, Nombre_Cliente, Documento_Identidad, Email, Telefono, Direccion, Contraseña, Numero_Tarjeta)
          VALUES
          (:ID_Usuario, :Nombre_Cliente, :Documento_Identidad, :Email, :Telefono, :Direccion, :Contraseña, :Numero_Tarjeta)";
    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);
    $statement->execute();
    $postId = $dbConn->lastInsertId();
    if($postId)
    {
      $input['ID_Cliente'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['ID_Cliente'];
  $statement = $dbConn->prepare("DELETE FROM usuario where ID_Cliente=:ID_Cliente");
  $statement->bindValue(':ID_Cliente', $id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $input = $_GET;
    $postId = $input['ID_Cliente'];
    $fields = getParams($input);

    $sql = "
          UPDATE usuario
          SET $fields
          WHERE ID_Cliente='$postId'
           ";

    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);

    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}



header("HTTP/1.1 400 Bad Request");

?>