<?php
include "config.php";
include "utils.php";


$dbConn =  connect($db);

/*
  listar todos los posts o solo uno
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['ID_Ingreso']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM ingreso_articulos where ID_Ingreso=:ID_Ingreso");
      $sql->bindValue(':ID_Ingreso', $_GET['ID_Ingreso']);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM ingreso_articulos");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $sql = "INSERT INTO ingreso_articulos
          (ID_Articulo, ID_Provedor, Fecha, Costo, Cantidad)
          VALUES
          (:ID_Articulo, :ID_Provedor, :Fecha, :Costo, :Cantidad)";
    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);
    $statement->execute();
    $postId = $dbConn->lastInsertId();
    if($postId)
    {
      $input['ID_Ingreso'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['ID_Ingreso'];
  $statement = $dbConn->prepare("DELETE FROM ingreso_articulos where ID_Ingreso=:ID_Ingreso");
  $statement->bindValue(':ID_Ingreso', $id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $input = $_GET;
    $postId = $input['ID_Ingreso'];
    $fields = getParams($input);

    $sql = "
          UPDATE ingreso_articulos
          SET $fields
          WHERE ID_Ingreso='$postId'
           ";

    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);

    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}


//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");

?>